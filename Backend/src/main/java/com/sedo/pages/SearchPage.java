package com.sedo.pages;

public class SearchPage extends BasePage {

    public String url = "/search?keyword=";

    public String url() {
        return super.url(url);
    }

    public void open() {
        super.open(url);
    }
}
