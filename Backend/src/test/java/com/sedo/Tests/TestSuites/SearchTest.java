package com.sedo.Tests.TestSuites;

import com.sedo.Tests.BaseTest;
import com.sedo.pages.SearchPage;
import io.restassured.response.Response;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;


public class SearchTest extends BaseTest {

    @DataProvider(name = "canSearchTheDomainsByKeywords")
    public Object[][] canSearchTheDomainsByKeywords() {
        return new Object[][]{
                {
                        "Weather"
                },
        };

    }

    @Test(dataProvider = "canSearchTheDomainsByKeywords")
    public void canSearchTheDomainByKeyWords(String keyword) {
        SearchPage searchPage = new SearchPage();
        Response response;

        response =
                given()
                        .contentType("application/json")
                        .get(searchPage.url() + keyword)
                        .then()
                        .extract()
                        .response();
        System.out.println(response.asString());
        response.then()
                .statusCode(200);

    }
}
