package com.sedo.Tests;

import com.codeborne.selenide.WebDriverRunner;
import com.sedo.automation.env_config.EnvConfig;
import io.restassured.RestAssured;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    public EnvConfig envConfig = ConfigFactory.create(EnvConfig.class, System.getProperties());

    @BeforeMethod
    public void setUp() {

        //RestAssured.baseURI = envConfig.url();
        RestAssured.port = 8080;
    }

    @AfterMethod
    public void tearDown() {
        if (WebDriverRunner.hasWebDriverStarted()) {
            WebDriverRunner.closeWebDriver();
        }
    }
}
