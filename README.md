# TestProject

''The tests can be run in two different ways'''

Info:

file contains the baseURL in the Util directory:

"utils/src/main/resources/env_config/env.properties".
How to run the tests

**Option 1**

1. open cmd.
2. switch to the directory where you want to clone the repository.
3. Clone the repository to your local environment with "https://gitlab.com/majidiqbal/testproject.git" command.
4. Switch to the project root directory "cd testproject"
4. run the following maven commands to run the tests
   
   "mvn clean test -Dsurefire.suiteXmlFiles=src/test/resources/suites/Test.xml"
5. validate the result in console.
   
**Option 2**

1. Follow the steps 1-3 from Option 1
2. Open intellij
3. import the project
4. open "src/test/resources/suites/" directory in desktop project.
5. right click on the Test.xml file and run the tests.
   
**OR**

1. Follow the steps 1-3 from Option 1
2. Open intellij
3. import the project
4. go to project desktop "/src/test/java/com/sedo/Tests/TestSuites/SearchTest.java"
5. run the whole class or individual tests (When running individual tests, tests should be run based on priority (Prio.1 should be run before prio. 2 and so on) as the tests are dependent on createBooking test.